
\documentclass[xcolor=table]{beamer}
\usepackage{hyperref}
\usepackage{amsfonts}
\usetheme{Berlin}
\usecolortheme{seahorse}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\begin{document}

\title % (optional, only for long titles)
{CS1026 -- Digital Logic Design}
\subtitle{Last one -- Inspection Design Methods}
\author % (optional, for multiple authors)
{Shane SheehanS \inst{1}}
\institute[Trinity Colege Dublin] % (optional)
{
  \inst{1}%
  ADAPT\\
  Trinity College Dublin
}

\date[2015] % (optional)
{\today}
\subject{Computer Science}

\frame{\titlepage}

\begin{frame}
\frametitle{Today's Overview}
%\tableofcontents[currentsection]
\tableofcontents
\end{frame}

\section{Classical Design}

\begin{frame}[allowframebreaks]{Limitations with Classical Design}
We can only use the classical design method with:

\begin{itemize}
\item A small number of inputs
\item States
\item Outputs
\end{itemize} 

The K-maps required become too difficult to draw and work with.

\framebreak
The Inspection Design Method provides ways to write the excitation equation using:

\begin{itemize}
 \item A timing diagram
\item A state diagram
\item A ASM chart 
\end{itemize}

For a synchronous Finite State Machine. 

\framebreak
By observing or inspecting the present state (PS) and next state (NS) for each state variable:

\begin{itemize}
\item We can write the $D$, $T$ and $J-K$ excitation equations
\end{itemize}

The equations derived using inspections do not give minimum equations however.

\framebreak

We have two inspection methods:

\begin{enumerate}
\item The Set-Hold 1 Method
\item Clear-Hold 0 Method
\end{enumerate}

\framebreak

We use the following table to write D excitation equations directly from a state diagram, ASM chart or timing diagram.

\begin{figure}
\includegraphics[width=\textwidth]{img/table}
\end{figure}

\framebreak

The ``Set-Hold 1 Method'' obtains the D excitation equations for the 1s of each state variable (flip-flop outputs):

\begin{itemize}
\item $Di$ = $\sum$(PS.external input conditions for set) + $\sum$(PS.external input conditions for hold 1) for $i=1, 2, 3, \ldots$
\end{itemize}

\framebreak

The ``Set-Hold 0 Method'' can be used to obtain the D excitation equations for the 0s of each state variable (flip-flop outputs)

\begin{itemize}
\item $Di$ = $\sum$(PS.external input conditions for clear) + $\sum$(PS.external input conditions for hold 0) for $i=1, 2, 3, \ldots$
\end{itemize}

\framebreak

For both of the methods:

\begin{itemize}
 \item If we have not completely specified FSM meaning and some state values as don’t cares
\begin{itemize}
\item Enter them as such so that we can use them in later reduction
\end{itemize}
\end{itemize}

\end{frame}

\section{Example 1}

\begin{frame}[allowframebreaks]{Simple Example \cite{braun2014digital}}

Obtain the excitation equations for the following state diagram of a Mixed Moore-Mealy machine:

\begin{itemize}
\item State -- Y1Y2
\item Input -- STOP
\item Output -- Z0Z1
\end{itemize}

\framebreak

Our state chart:

\begin{figure}
\includegraphics[width=0.8\textwidth]{img/statedia}
\end{figure}


\framebreak

By inspecting all state transitions ($Y1=0 \implies Y1^+=1$) and all Hold 1 transitions ($Y1=1 \implies Y1^+=1$):

\begin{itemize}
\item We can write the D1 excitation equation:
\begin{itemize}
\item $D1 = Y1'.Y2 + Y1.Y2' + Y1.Y2.STOP$
\end{itemize}
\end{itemize}

\framebreak

Now repeat the previous step for D2 using Y2 transitions..

\begin{itemize}
\item By observing (or inspecting) all transitions ($Y2=0 \implies Y2^+ =1$) and all Hold 1 transitions ($Y2=1 \implies Y2^+ =1$) 
\begin{itemize}
\item We can write the D1 excitation equation: $D2 = Y1'.Y2'.STOP' + Y1.Y2' + Y1.Y2.STOP$
\end{itemize}

\end{itemize}

\framebreak

Alternatively..

We could also look for the 0’s function using Clear-hold 0 method to find $D1'$ and $D2'$

\framebreak

Based on the state diagram $Z0$

\begin{itemize}
\item We see that we have a Moore-type output 
\begin{itemize}
\item It only depends on the state variables (flip-flop outputs)
\end{itemize}
\end{itemize}

\framebreak

\begin{figure}
\includegraphics[width=0.32\textwidth]{img/kmap1}
\end{figure}

We will use a K-map with state variables to find minimized the $Z0$ equation

\framebreak

$Z1$ is a Mealy-type output since it depends on both the state variables and external input

\begin{figure}
\centering
\includegraphics[width=0.32\textwidth]{img/kmap2}
\end{figure}

Again we use a K-map with state variables plus external input to find minimize $Z1$ equation

\end{frame}

\section{Example 2}

\begin{frame}[allowframebreaks]{Real World Example}

\begin{block}{A question}
Design a synchronous 2-bit Binary up down counter that counts up when input signal $X=0$ and counts down when input signal $X=1$
\end{block}

\begin{itemize}
\item State -- $Y1Y2$
\item Input -- $X$
\end{itemize}

\framebreak

The state chart:

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/statedia2}
\end{figure}

\framebreak

Use the \emph{Set Clear Method} to obtain the J-K excitation equations for the $1$s of each state variable (flip-flop outputs):

\begin{itemize}
\item By observing all the sets ($Y1=0 \implies Y1^+=1$), we can write the J1 excitation equation:
\begin{itemize}
\item $J1 = Y1'.Y2.X' + Y1'.Y2'.X = Y2'.X + Y2.X'$
\end{itemize}
\end{itemize}

\framebreak

Also..

\begin{itemize}
\item By observing all the clears ($Y1=1 \implies Y1^+=0$), we can write the K1 excitation equation:

\begin{itemize}
\item $K1 = Y1.Y2'.X + Y1.Y2.X' = Y2'.X + Y2.X'$
\end{itemize}
\end{itemize}

\framebreak

Now we repeat this for the second Flip Flop:

\begin{itemize}
\item By observing all the sets ($Y2=0 \implies Y2^+ =1$), we can write the J2 excitation equation:

\begin{itemize}
\item $J2 = Y1'.Y2'.X' + Y1'.Y2'.X + Y1.Y2'.X' + Y1.Y2'.X = Y1'.Y2' + Y1.Y2' = Y2'$
\end{itemize}
\end{itemize}

\framebreak

Finally:

\begin{itemize}
\item By observing all the clears ($Y2 = 1$, $Y2 = 0$), we can write the K2 excitation equation:

\begin{itemize}
\item $K2 = Y1'.Y2.X' + Y1'.Y2.X + Y1.Y2.X' + Y1.Y2.X = Y1'.Y2 + Y1.Y2 = Y2$
\end{itemize}
\end{itemize}

\end{frame}

\section{Q\&A}

\begin{frame}[allowframebreaks]
        \frametitle{References (Homework)}
         \bibliographystyle{apalike}
      \bibliography{ref}
\end{frame}
% etc


\end{document}
