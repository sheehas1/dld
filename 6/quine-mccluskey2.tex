\documentclass[xcolor=table]{beamer}
\usepackage{hyperref}
\usetheme{Berlin}
\usecolortheme{seahorse}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\begin{document}

\title % (optional, only for long titles)
{CS1026 -- Digital Logic Design}
\subtitle{A slow introduction to the Quine-McCluskey Algorithm I}
\author % (optional, for multiple authors)
{Shane Sheehan \inst{1}}
\institute[Trinity College Dublin] % (optional)
{
  \inst{1}%
  ADAPT\\
  Trinity College Dublin
}

\date[2015] % (optional)
{\today}
\subject{Computer Science}

\frame{\titlepage}

\begin{frame}
\frametitle{Today's Overview}
%\tableofcontents[currentsection]
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}[allowframebreaks]{Quine-McCluskey \cite{majumder2015investigation}}

\begin{itemize}
\item An exact algorithm which finds a minimum-cost sum-of-products implementation of a Boolean function 
\item This lecture partially introduces the method
\end{itemize}

\framebreak

Steps of the Quine-McCluskey algorithm:
\begin{enumerate}

\item Generate Prime Implicants
\item Construct Prime Implicant Table
\item Reduce Prime Implicant Table

\begin{enumerate}
\item Remove Essential Prime Implicants
\item Row Dominance
\item \emph{Column Dominance}
\end{enumerate}

\item Solve Prime Implicant Table
\end{enumerate}

\framebreak

\end{frame}

\section{Row Dominance}
\begin{frame}[allowframebreaks]{Row Dominance}
Consider the following Karnaugh map of a 4-input Boolean function: $F(A,B,C,D)= \sum{m}(1,2,3,5,7) + \sum{d}(0, 6, 9, 13)$

\framebreak

The Karnaugh-Map!

\begin{figure}
\centering
\includegraphics[width=0.48\textwidth]{img/kmap.pdf}
\end{figure}

$F(A,B,C,D)= \sum{m}(1,2,3,5,7) + \sum{d}(0, 6, 9, 13)$

\framebreak

\begin{figure}
\centering
\includegraphics[width=0.48\textwidth]{img/kmap.pdf}
\end{figure}

\begin{itemize}
\item We have $4$ prime implicants: A'B', C'D, A'D and A'C.
\item None denotes an essential prime implicant
\end{itemize}

\framebreak

\begin{figure}
\centering
\includegraphics[width=0.48\textwidth]{img/kmap.pdf}
\end{figure}

Our task:

\begin{itemize}
\item Pick a minimum subset of these implicants..
\item ..to cover the 5 ON-set min-terms
\end{itemize}

\framebreak

The prime implicant table for the Karnaugh map:

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

\begin{itemize}
\item 4 prime implicants listed as columns
\item 5 ON-set min-terms are listed as rows
\end{itemize}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

\begin{itemize}
\item Row 2 covered by two of these three columns: A'B' and A'C
\item Row 7 also covered by two of these three columns: A'D and A'C
\end{itemize}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

\begin{itemize}
\item Any prime implicant which contains row 2 also contains row 3
\item Any prime implicant which contains row 7 also contains row 3
\end{itemize}

We can ignore the covering of row 3: \emph{we cover row 2 or row 7!}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

\begin{itemize}
\item Row 3 row dominates row 2 
\item Row 3 row dominates row 7
\end{itemize}

The situation is now the reverse of column dominance: \emph{we cross out the dominating (larger) row}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
\rowcolor[HTML]{C0C0C0} 
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}
\begin{block}{In this case..}
\begin{itemize}
\item We can cross row 3 crossed out
\item We no longer need to worry about it
\end{itemize}
\end{block}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
\rowcolor[HTML]{C0C0C0} 
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
\rowcolor[HTML]{C0C0C0} 
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

\begin{block}{But also..}
\begin{itemize}
\item Row 1 dominates row 5
\item We can cross row 1 out!
\end{itemize}
\end{block}

\framebreak

\begin{table}[]
\centering
\begin{tabular}{l|cccc}
  & \begin{tabular}[c]{@{}c@{}}A'B'\\ (1,2,3)\end{tabular} & \begin{tabular}[c]{@{}c@{}}C'D\\ (1,5)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'D\\ (1,3,5,7)\end{tabular} & \begin{tabular}[c]{@{}c@{}}A'C\\ (2,3,7)\end{tabular} \\ \hline
\rowcolor[HTML]{C0C0C0} 
1 & X                                                      & X                                                   & X                                                       &                                                       \\
2 & X                                                      &                                                     &                                                         & X                                                     \\
\rowcolor[HTML]{C0C0C0} 
3 & X                                                      &                                                     & X                                                       & X                                                     \\
5 &                                                        & X                                                   & X                                                       &                                                       \\
7 &                                                        &                                                     & X                                                       & X                                                    
\end{tabular}
\end{table}

But why?

\begin{itemize}
\item Row 1 covered by row 5
\end{itemize}

\end{frame}

\section{Outstanding issues}
\begin{frame}{Does this really replace Karnaugh  Maps?}

So far we have just used Karnaugh Maps to help us find prime implicants

\begin{itemize}
\item K-Maps good at this for functions with less than 5 Boolean values
\end{itemize}

\begin{block}{However.. let's minimise:}
\begin{small}
%should use align or something... oh well..
$F(A, B, C, D, E, F, G, H, I, J) =$\\
$\sum{m}(61, 63, 125, 127, 450, 593, 595, 625, 627, 721, 723, 753, 755, 874, 875,$\\ $878, 879, 957, 959, 1021, 1023)$
\end{small}
\end{block}

emmm.. help! :'(

\end{frame}

\begin{frame}{We need a better method..}

.. To find prime implicants

\begin{block}{Abstract Method}

\begin{enumerate}

\item List Min-terms (sorted by number of $1$'s)
\item Combine Pairs of Min-terms
\item Combine Pairs of Products
\end{enumerate}

\end{block}

\end{frame}

\begin{frame}{Putting it all together}
Next time.. time for more tips about the lab! (zzzzz...)

\begin{block}{Any Problems?}
\begin{itemize}
\item Ask!
\item E-Mail: \emph{sheehas1@scss.tcd.ie}
\end{itemize}
\end{block}

\end{frame}

\begin{frame}[allowframebreaks]
        \frametitle{References (Homework)}
        
         \bibliographystyle{apalike}
      \bibliography{ref}
\end{frame}
% etc

\end{document}
