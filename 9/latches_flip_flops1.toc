\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Flip-flops vs Latches}{6}{0}{2}
\beamer@sectionintoc {3}{Flip-flop/Latch Taxonomy}{9}{0}{3}
\beamer@sectionintoc {4}{A Simple Set-Reset Latch}{11}{0}{4}
