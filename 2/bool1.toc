\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Logic Gates}{3}{0}{1}
\beamer@sectionintoc {2}{Axioms/Postulates}{6}{0}{2}
\beamer@sectionintoc {3}{Principle of Duality}{8}{0}{3}
\beamer@sectionintoc {4}{Boolean Functions}{9}{0}{4}
\beamer@sectionintoc {5}{Boolean Algebra Theorems}{12}{0}{5}
