\documentclass[xcolor=table]{beamer}
\usepackage{hyperref}
\usepackage{amsfonts}
\usetheme{Berlin}
\usecolortheme{seahorse}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\begin{document}

\title % (optional, only for long titles)
{CS1026 -- Digital Logic Design}
\subtitle{Sequential Logic Analysis}
\author % (optional, for multiple authors)
{Shane Sheehan \inst{1}}
\institute[Trinity College Dublin] % (optional)
{
  \inst{1}%
  ADAPT\\
  Trinity College Dublin
}

\date[2015] % (optional)
{\today}
\subject{Computer Science}

\frame{\titlepage}

\begin{frame}
\frametitle{Today's Overview}
%\tableofcontents[currentsection]
\tableofcontents
\end{frame}

\section{FSM -- A quick recap}

\begin{frame}[allowframebreaks]{Synchronous Finite State Machine Design}

Common Examples of Synchronous FSM
\begin{itemize}
\item Up and Down Binary Counters
\item Shift Registers
\item Sequence Detectors
\item Controllers
\end{itemize}

\framebreak

We use the:

\begin{itemize}
 \item Seven-Step Design Process for Synchronous Sequential Design 
\item Also called \emph{Classical Design}
\end{itemize}

\framebreak

Organising the Design Specifications –Use one or more of the following tools:
\begin{itemize}
\item Timing Diagram
\item State Diagram 
\item ASM Chart
\end{itemize}

\framebreak
Determine the number of flip-flops based on the number of states

\begin{itemize}
\item You may use full encoding or \emph{one-hot} encoding
\item Full encoding utilise all possible combinations of the flip-flops
\end{itemize}

\begin{block}{How many flip flops?}
  To decide the number of flip-flops we use the following inequality: $2^n \geq $ No. of States
\end{block}

\framebreak
\emph{One-hot} encoding $\implies$ when the flip-flop has an output $1$

\vspace{1cm}

Thus:
\begin{itemize}
 \item The number of flip-flops $=$ No. of States
\item Once we know the number of flip-flops
\begin{itemize}
 \item Assign one variable for each of the flip-flop output
\end{itemize}
\end{itemize}

\framebreak

\begin{figure}
\includegraphics[width=0.64\textwidth]{img/statedia}
\end{figure}

Assign a unique code to each state:

\begin{itemize}
\item A specific value for present state variables
\end{itemize}

\framebreak
Select the flip-flop type and draw the Present State/Next State (PS/NS) table:

\begin{figure}
\includegraphics[width=0.8\textwidth]{img/psns}
\end{figure}

\begin{itemize}
\item This determines the excitation input equations and the Moore and/or Mealy output equations.
\end{itemize}

\framebreak

Remember the excitation input and next state relationship flip-flops:

\begin{itemize}
\item D flip-flops: $D = Y^+$ or $Y^+ = D$
\item J-K flip-flops: $J = Y^+$ or $K=Y^{+\prime}$ or $Y^+ = JY^{\prime} + K^{\prime}Y$
\item T flip-flops:  $T = Y^+ \oplus Y$ or $Y^+ = T \oplus Y$
\end{itemize}

\framebreak

The other \emph{more practical} stages

\begin{itemize}
\item Draw the circuit schematic (paper or CAD tool).
\item Perform a simulation to test the functionally of the design
\item Implement the design with hardware
\end{itemize}
\end{frame}

\section{Example 1}

\begin{frame}[allowframebreaks]{Ripple Carry Output Counter \cite{nelson1995digital}}

\begin{block}{Our Task}
\begin{itemize}
\item Design a 2-bit binary up-counter with a ripple carry output ($RCO$) using D flip-flops
\item Note: The input CLR' denotes an asynchronous input that overrides the clock
\end{itemize}
\end{block}

\framebreak

A timing diagram helps..

\begin{figure}
\includegraphics[width=0.8\textwidth]{img/timing}
\end{figure}

\framebreak

A few things to remember:

\begin{enumerate}
\item $T_{clk}$ is the clock period and ST is the state time.
\item The first two events are less and then more than $T_{clk}$.
\item $Y1Y2$ are the states (counts).
\item $RCO$ is a Moore output indicating when the maximum count has been reached.
\end{enumerate}

\framebreak

Although a timing diagram appears more complete, we can understand state diagram better: 

\begin{figure}
\includegraphics[width=0.64\textwidth]{img/count}
\end{figure}


\begin{itemize}
\item Since it does not contain the clock timing information.
\end{itemize}

\framebreak

We can use a PS/NS table to describe the functionality (Step 1):

\begin{figure}
\includegraphics[width=0.8\textwidth]{img/psns}
\end{figure}


\begin{block}{Top hint}
The \emph{Next state, NS} defines the state of the machine 
\begin{itemize}
\item During the next clock cycle
\end{itemize}
\end{block}

\framebreak

Step 2:
\begin{itemize}
\item Determine the no. of flip-flops based on the no. of states
\item For full encoding -- no. of states = 4 $\leq 2^2$
\end{itemize}

Step 3:

\begin{itemize}
\item Assign Unique code to each state
\begin{itemize}
\item We already did this in the state diagram
\end{itemize}
\end{itemize}

\framebreak

Step 4:
\begin{itemize}
\item Write the excitation-input equations
\end{itemize}

\begin{block}{Remember}
The D flip-flop excitation equation: $D = Y^+$
\end{block}

\framebreak

More Step 4:

\begin{figure}
\includegraphics[width=0.48\textwidth]{img/kmap}
\end{figure}

\begin{itemize}
\item The K-map for each of the desired outputs: $Y1^+$, $Y2^+$, $RCO$ helps us determine excitation-input equations
\end{itemize}

\framebreak
Last bit of step 4:

\begin{figure}
\includegraphics[width=0.32\textwidth]{img/comp_kmap}
\end{figure}

\begin{itemize}
\item Draw a composite K-map for each of the desired outputs:
\begin{itemize}
 \item $Y1$, $Y2$, $RCO$
\end{itemize}
\end{itemize}

\framebreak
Now relax.. (Step 5)

\begin{figure}
\includegraphics[width=0.64\textwidth]{img/circ}
\end{figure}

\begin{itemize}
\item Draw the Circuit Schematic
\end{itemize}

\framebreak

We skip steps 6 and 7

\begin{itemize}
\item Remember to do them for labs
\item And note them when doing exams
\begin{itemize}
\item It shows understanding!
\end{itemize}
\end{itemize}

\end{frame}

\section{Optional Homework.. :-O}

\begin{frame}{Some for you to try}

\begin{block}{Example Style Question}
Design a synchronous sequential circuit called \emph{Div-by-3} having an output $Z$ that divides the system clock frequency $f$ CLK by $3$. 
\begin{itemize}
\item Use an output duty cycle of two-thirds
\begin{itemize}
\item 2 CLK cycle high, 1 cycle low
\end{itemize}
\item Design the circuit using positive-edge-triggered flip-flops 
\end{itemize}

\end{block}

\end{frame}


\begin{frame}[allowframebreaks]
        \frametitle{References (Homework)}
         \bibliographystyle{apalike}
      \bibliography{ref}
\end{frame}
% etc


\end{document}
